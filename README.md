# Clinostat Control Board

Design files for the clinostat. All files are designed by Lovrenc Košenina. This is part of the [Open Source Clinostat](https://tranxxenolab.net/projects/clinostat/).

All files are licensed under the GPLv3.


# Support

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance.

Created during a [Biofriction](https://biofriction.org/) residency.

Produced by [Galerija Kapelica](http://kapelica.org/)/[Zavod Kersnikova](https://kersnikova.org/) in Ljubljana, Slovenia.

Biofriction is supported by the European Commission--Creative Europe.
